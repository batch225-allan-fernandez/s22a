

let registeredUsers = [

"Nana Banana",
"Granger Baboy",
"Macy William",
"Claude",
"Sheldon Pogi",
"Bruce Sipunin",
"Dwarfino"
];

let friendsList = [];



// Registering new user and checking if username has already taken
function register(username) {

    let username1 = registeredUsers.includes(username);
    if(username1 == true){
        alert("Registration failed. Username already exists!");
    }
    else {
        let usernameAdded = registeredUsers.push(username);
        alert("Thank you for registering!")
    }
}


// Adding new friend from registeredUsers array
function addFriend(newFriend) {
    let newFriend1 = registeredUsers.includes(newFriend);
    if(newFriend1 == true){
        let foundUser = friendsList.push(newFriend);
        alert("You have added " + newFriend + " as a friend!");
    }
    else {
        alert("User not found.");
    }
}



// This will show friends one by one vertically if there's any
function Friends(){
    if (friendsList == 0){
        alert("You currently have 0 friends. Add one first.");
        console.log(friendsList);
    }
    else {
        friendsList.forEach(function(friend){
            console.log(friend);
        })
    }
}


//This will count how many friends do you have from friendslist
function countFriends(){

    if(friendsList == 0){
        alert("You currently have 0 friends. Add one first.")
    }
    else{
        let numberOfFriends = friendsList.push("")
        console.log("You currently have "+ (numberOfFriends-1) + " friend/s");
        let deletedNull = friendsList.pop();
    }
}



// This will delete your last friend from friendslist
function deleteFriend(){
    if (friendsList == 0){
        alert("You currently have 0 friends. Add one first.");
    }
    else{
        let deletedFriend = friendsList.pop();
        alert("You have deleted " + deletedFriend);
        console.log(deletedFriend + " was deleted from your friend list.");

    }
};


// Stretch Goal: Deleting specific friend from friendslist
function deleteSpecificFriend(index, deleteCount){
    let deleteSpecificFriend1 = friendsList.splice(index, deleteCount);
    console.log("You unfriended: " + deleteSpecificFriend1);
    console.log("Remaining Friends: " + friendsList);
};